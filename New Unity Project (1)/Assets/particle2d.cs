﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class particle2d : MonoBehaviour
{
    //Step 1
    public Vector2 position, velocity, acceleration;
    public float rotation, angularVelocity, angularAcceleration;

    //Step 2
    void updatePositionExplicitEuler(float dt)
    {
        // x(t+dt) = x(t) + v(t)dt
        //Euler:
        // F(t+dt) = F(t) + f(t)dt
        //                + (dF/dt) dt
        position += velocity * dt;

        // v(t+dt) = v(t) + a(t)dt
        // updating velocity should always be after updating position, never before
        velocity += acceleration * dt;
    }

    void updatePositionKinematic(float dt)
    {
        position += velocity * dt + 0.5f * acceleration * dt * dt;
        velocity += acceleration * dt;
    }

    void updateRotationExplicitEuler(float dt)
    {
        rotation += angularVelocity * dt;
        angularVelocity += angularAcceleration * dt;
    }

    void updateRotationKinematic(float dt)
    {
        rotation += angularVelocity * dt + 0.5f * angularAcceleration * dt * dt;
        angularVelocity += angularAcceleration * dt;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Step 3
        // choose integrator
        //updatePositionExplicitEuler(Time.fixedDeltaTime);
        updatePositionKinematic(Time.fixedDeltaTime);

        // choose rotation integrator
        //updateRotationExplicitEuler(Time.fixedDeltaTime);
        updateRotationKinematic(Time.fixedDeltaTime);
        // update the game object's position
        transform.position = position;
        transform.Rotate(new Vector3(transform.rotation.x, transform.rotation.y, rotation));


        //Step 4
        // test by faking motion along a curve
        acceleration.x = -Mathf.Sin(Time.time);
        //angularAcceleration = -Mathf.Sin(Time.time);
        //acceleration.y = -Mathf.Sin(Time.time * 0.01f);
    }
}
